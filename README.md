# Projectes 2018

## Escola del Treball de Barcelona

### ASIX Administració de sistemes informàtics en xarxa

#### Curs 2017-2018

#### M14 Projecte


1) Roger Ferran: **Projecte RDBMS Vs NoSQL**

GIT: https://github.com/isx45128227/MongoVsPostgres  
https://docs.google.com/document/d/15x27C-I5PMr4O-_WLwK5vrhxAtbGLgozVvZQrPBdQyw/edit?usp=sharing_eil&ts=5ad5b315 


2) Marc Serra Hidalgo: **GUI-Tool for Linux System Administration**

https://gitlab.com/isx41745190/guitool 


3) Vladimir Remar : **Ghandi Reload**

https://gitlab.com/vladimir-remar/Gandhi.Reload 

4) Tania Gabriela Álvarez: **Servei Kerberos**

https://github.com/isx434324/kerberosproject 


5) Adrian Davila Montañez: **Cloud Computing**

https://gitlab.com/isx48102233/cloudcomputing


6) Nick Inga : **LDAP GSSAPI**
https://github.com/isx26067826/project 


7) José Luis Pérez: **Serveis de vídeo**

https://gitlab.com/jooseluisperez/DMC/


8) Arnau Esteban Contreras: **Sistema Radius**
 
https://github.com/isx47590131/radius
